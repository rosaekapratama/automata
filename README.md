# Automata

Automatic configuration for spring data source, kafka, and redis

Below are properties example which u will put in your application.yml

```
automata:
  datasources:                                                                          #you can set more than one data source here
    - name: datasource1                                                                 #this name will be the data source bean name
      jdbc-url: jdbc:oracle:thin:@localhost:1521/AUTOMATA_1                             #set the jdbc url here
      username: TAMA                                                                    #set username here for authentication
      password: VERTIGO                                                                 #set password here for authentication
      minimum-idle: 5                                                                   #minimum idle connection
      maximum-pool-size: 50                                                             #maxium connection pool size
      data-source-properties."[v$session.program]": automata                            #custom for oracle
    - name: datasource2
      jdbc-url: jdbc:oracle:thin:@localhost:1521/AUTOMATA_2
      username: TAMA
      password: VERTIGO
      minimum-idle: 5
      maximum-pool-size: 50
      data-source-properties."[v$session.program]": automata
  entity-manager-factories:
    - name: entityManagerFactory1                                                       #this name will be the entity manager factory bean name
      datasource-ref: datasource1                                                       #data source reference bean name which you set above
      packages:
        - com.firstrose.automata.entity.one                                             #entity package which you need to scan
    - name: entityManagerFactory2
      datasource-ref: datasource2
      packages:
        - com.firstrose.automata.entity.two
  transaction-managers:
    - name: transactionManager1                                                         #this name will be the transaction manager bean name
      entity-manager-factory-ref: entityManagerFactory1
    - name: transactionManager2
      entity-manager-factory-ref: entityManagerFactory2
      
  kafka:
    listener-container-factories:
      - name: listenerContainerFactories                                                #this name will be the listener container factory bean name
        bootstrap-servers: localhost:9092
        key-deserializer: org.apache.kafka.common.serialization.StringDeserializer
        value-deserializer: org.apache.kafka.common.serialization.StringDeserializer

    templates:
      - name: kafkaTemplate                                                             #this name will be the kafka listener bean name
        bootstrap-servers: localhost:9092
        key-serializer: org.apache.kafka.common.serialization.StringSerializer
        value-serializer: org.springframework.kafka.support.serializer.JsonSerializer
```


### How to integrate with JPA


Example configuration class
```
@Configuration
@EnableTransactionManagement
@EnableAutomataDatasource
@EnableJpaRepositories(
        basePackages = "com.firstrose.automata.repository",
        entityManagerFactoryRef = "entityManagerFactory1",
        transactionManagerRef = "transactionManager1"
)
public class AutomataJpaConfiguration {
}
```

`@EnableAutomataDatasource` This annotation is to tell automata to create bean on each data sources, entity manager factories, and transaction manager
which we create in application.yml under `automata.*`

```
@EnableJpaRepositories(
        basePackages = "com.firstrose.automata.repository",
        entityManagerFactoryRef = "entityManagerFactory1",
        transactionManagerRef = "transactionManager1"
)
``` 
In this JPA annotation, we put both `entityManagerFactory1` and `transactionManager1` bean name which we already set on application.yml above under `automata.*` to `entityManagerFactoryRef` and `transactionManagerRef`