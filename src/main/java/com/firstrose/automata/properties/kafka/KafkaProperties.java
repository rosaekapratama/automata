package com.firstrose.automata.properties.kafka;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class KafkaProperties {
    private List<ListenerContainerFactoryProperties> listenerContainerFactories;
    private List<TemplateProperties> templates;
}
