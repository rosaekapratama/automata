package com.firstrose.automata.properties.kafka;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ListenerContainerFactoryProperties {

    private String name;
    private String bootstrapServers;
    private String groupId;
    private String keyDeserializer;
    private String valueDeserializer;
    private String keyJsonType;
    private String valueJsonType;
    private String autoOffsetRest;
}
