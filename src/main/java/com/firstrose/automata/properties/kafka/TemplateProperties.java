package com.firstrose.automata.properties.kafka;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TemplateProperties {

    private String name;
    private String bootstrapServers;
    private String clientId;
    private String keySerializer;
    private String valueSerializer;
}
