package com.firstrose.automata.properties;

import com.firstrose.automata.properties.kafka.KafkaProperties;
import com.firstrose.automata.properties.persistence.DataSourceProperties;
import com.firstrose.automata.properties.persistence.EntityManagerFactoryProperties;
import com.firstrose.automata.properties.persistence.TransactionManagerProperties;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;


import java.util.List;

@Getter
@Setter
@ConfigurationProperties(prefix = "automata")
public class AutomataProperties {

    private List<DataSourceProperties> datasources;
    private List<EntityManagerFactoryProperties> entityManagerFactories;
    private List<TransactionManagerProperties> transactionManagers;
    private KafkaProperties kafka;

}
