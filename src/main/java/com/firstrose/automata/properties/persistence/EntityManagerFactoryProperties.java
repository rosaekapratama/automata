package com.firstrose.automata.properties.persistence;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateProperties;

import java.util.List;

@Getter
@Setter
public class EntityManagerFactoryProperties extends HibernateProperties {

    private String name;
    private String datasourceRef;
    private List<String> packages;
}
