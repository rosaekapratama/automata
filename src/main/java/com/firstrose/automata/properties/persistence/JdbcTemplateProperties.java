package com.firstrose.automata.properties.persistence;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JdbcTemplateProperties {

    private String name;
    private String datasourceRef;

}
