package com.firstrose.automata.properties.persistence;

import com.zaxxer.hikari.HikariConfig;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DataSourceProperties extends HikariConfig {

    private String name;
}
