package com.firstrose.automata.configuration;

import com.firstrose.automata.properties.AutomataProperties;
import lombok.SneakyThrows;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.ApplicationContext;

public abstract class BaseAutoConfiguration implements BeanPostProcessor {

    @Autowired
    protected ApplicationContext applicationContext;

    @Autowired
    protected AutomataProperties automataProperties;

    protected void populateObject(String beanName, Object beanObject) {
        ConfigurableListableBeanFactory configurableListableBeanFactory = (ConfigurableListableBeanFactory) applicationContext.getAutowireCapableBeanFactory();
        configurableListableBeanFactory.registerSingleton(beanName, beanObject);
        configurableListableBeanFactory.initializeBean(beanObject, beanName);
        configurableListableBeanFactory.autowireBean(beanObject);
    }

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        return bean;
    }

    @SneakyThrows
    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if(beanName.equals("tomcatServletWebServerFactory")) {
            init();
        }
        return bean;
    }

    public abstract void init() throws Exception;
}
