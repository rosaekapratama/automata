package com.firstrose.automata.configuration;

import com.firstrose.automata.annotation.EnableAutomataKafka;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class BaseKafkaAutoConfiguration extends BaseAutoConfiguration {

    protected List<EnableAutomataKafka> searchAnnotation(String[] beanNames) {
        List<EnableAutomataKafka> annotations = new ArrayList<>();
        for(String beanName : beanNames)
            annotations.add(applicationContext.findAnnotationOnBean(beanName, EnableAutomataKafka.class));
        return annotations;
    }

    protected Boolean filterIsOn(List<EnableAutomataKafka> annotations) {
        for(EnableAutomataKafka annotation : annotations) {
            if(annotation.listenerContainerFactories().length>0)
                return true;
        }
        return false;
    }

    protected Boolean beanIsExist(String beanName, List<EnableAutomataKafka> annotations) {
        for(EnableAutomataKafka annotation : annotations) {
            List<String> dsNameList = Arrays.asList(annotation.templates());
            if(dsNameList.contains(beanName)) {
                return true;
            }
        }
        return false;
    }
}
