package com.firstrose.automata.configuration;

import com.firstrose.automata.properties.AutomataProperties;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class AutomataBeanFactoryPostProcessor implements ApplicationContextAware {

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        ConfigurableListableBeanFactory configurableListableBeanFactory = (ConfigurableListableBeanFactory) applicationContext.getAutowireCapableBeanFactory();
        AutomataProperties automataProperties = configurableListableBeanFactory.createBean(AutomataProperties.class);
        configurableListableBeanFactory.autowireBean(automataProperties);
    }
}
