package com.firstrose.automata.configuration;

import com.firstrose.automata.annotation.EnableAutomataDatasource;
import com.firstrose.automata.annotation.Source;
import com.firstrose.automata.properties.persistence.DataSourceProperties;
import com.firstrose.automata.properties.persistence.EntityManagerFactoryProperties;
import com.firstrose.automata.properties.persistence.JdbcTemplateProperties;
import com.firstrose.automata.properties.persistence.TransactionManagerProperties;
import com.zaxxer.hikari.HikariDataSource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

@Slf4j
public class DataSourceAutoConfiguration extends BaseAutoConfiguration {

    private List<EnableAutomataDatasource> searchAnnotation(String[] beanNames) {
        List<EnableAutomataDatasource> annotations = new ArrayList<>();
        for(String beanName : beanNames)
            annotations.add(applicationContext.findAnnotationOnBean(beanName, EnableAutomataDatasource.class));
        return annotations;
    }

    private Boolean filterIsOn(List<EnableAutomataDatasource> annotations) {
        for(EnableAutomataDatasource annotation : annotations) {
            if(!annotation.name().isEmpty() || annotation.sources().length>0)
                return true;
        }
        return false;
    }

    private Boolean beanIsExist(String beanName, List<EnableAutomataDatasource> annotations, List<String> entityPackages) {
        for(EnableAutomataDatasource annotation : annotations) {
            if(annotation.name().equals(beanName)) {
                entityPackages.addAll(Arrays.asList(annotation.entityPackage()));
                return true;
            }else {
                for(Source source : annotation.sources())
                    if(source.name().equals(beanName)) {
                        entityPackages.addAll(Arrays.asList(source.entityPackage()));
                        return true;
                    }
            }
        }
        return false;
    }

    public void init() {
        // If automata data source properties is not exists in application properties then complete init
        List<DataSourceProperties> dataSourcePropertiesList = automataProperties.getDatasources();
        if(dataSourcePropertiesList == null)
            return;

        // Continue only if any automata data source exists in application properties
        String[] beanNames = applicationContext.getBeanNamesForAnnotation(EnableAutomataDatasource.class);
        List<EnableAutomataDatasource> annotations = searchAnnotation(beanNames);

        for(DataSourceProperties dsProperties : dataSourcePropertiesList) {
            List<String> entityPackages = new ArrayList<>();
            String dsBeanName = dsProperties.getName();
            Boolean filterIsOn = filterIsOn(annotations);
            Boolean isExist = beanIsExist(dsBeanName, annotations, entityPackages);

            if(beanNames.length>0 && (!filterIsOn || isExist)) {
                log.info("Registering data source '{}'", dsBeanName);
                HikariDataSource dsObject = new HikariDataSource(dsProperties);
                populateObject(dsBeanName, dsObject);
                initEntityManagerFactories(dsBeanName, dsObject, entityPackages);
                initJdbcTemplates(dsBeanName, dsObject);
            }
        }
    }

    private void initEntityManagerFactories(String dsName, HikariDataSource dsObject, List<String> entityPackages) {
        // If automata entity manager factory properties is not exists in application properties then complete init
        List<EntityManagerFactoryProperties> entityManagerFactoryPropertiesList = automataProperties.getEntityManagerFactories();
        if(entityManagerFactoryPropertiesList == null)
            return;

        // Continue only if any automata entity manager factory exists in application properties
        for(EntityManagerFactoryProperties emfProperties : entityManagerFactoryPropertiesList) {
            if(!emfProperties.getDatasourceRef().equals(dsName))
                continue;

            String emfBeanName = emfProperties.getName();
            log.info("Registering entity manager factory '{}'", emfBeanName);
            LocalContainerEntityManagerFactoryBean emfObject = new LocalContainerEntityManagerFactoryBean();
            emfObject.setPersistenceUnitName(emfBeanName);
            emfObject.setDataSource(dsObject);
            JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
            emfObject.setJpaVendorAdapter(vendorAdapter);
            entityPackages.addAll(emfProperties.getPackages());
            emfObject.setPackagesToScan(entityPackages.toArray(new String[0]));
            Properties properties = new Properties();
            if(emfProperties.getDdlAuto()!=null)
                properties.setProperty("hibernate.hbm2ddl.auto", emfProperties.getDdlAuto());
            else
                properties.setProperty("hibernate.hbm2ddl.auto", "none");
            emfObject.setJpaProperties(properties);
            populateObject(emfBeanName, emfObject);
            initTransactionManagers(emfBeanName, emfObject);
        }
    }

    private void initTransactionManagers(String emfName, LocalContainerEntityManagerFactoryBean emfObject) {
        // If automata transaction manager properties is not exists in application properties then complete init
        List<TransactionManagerProperties> transactionManagerPropertiesList = automataProperties.getTransactionManagers();
        if(transactionManagerPropertiesList == null)
            return;

        // Continue only if any automata transaction manager exists in application properties
        for(TransactionManagerProperties tmProperties : transactionManagerPropertiesList) {
            if(!tmProperties.getEntityManagerFactoryRef().equals(emfName))
                continue;

            String tmBeanName = tmProperties.getName();
            log.info("Registering transaction manager '{}'", tmBeanName);
            JpaTransactionManager tmObject = new JpaTransactionManager();
            tmObject.setEntityManagerFactory(emfObject.getObject());
            populateObject(tmBeanName, tmObject);
        }
    }

    private void initJdbcTemplates(String dsName, HikariDataSource dsObject) {
        // If automata jdbc template properties is not exists in application properties then complete init
        List<JdbcTemplateProperties> jdbcTemplatePropertiesList = automataProperties.getJdbcTemplateProperties();
        if(jdbcTemplatePropertiesList == null)
            return;

        // Continue only if any automata jdbc template exists in application properties
        for(JdbcTemplateProperties jtProperties : jdbcTemplatePropertiesList) {
            if(!jtProperties.getDatasourceRef().equals(dsName))
                continue;

            String jtBeanName = jtProperties.getName();
            log.info("Registering jdbc template '{}'", jtBeanName);
            JdbcTemplate jtObject = new JdbcTemplate(dsObject);
            populateObject(jtBeanName, jtObject);
        }
    }
}
