package com.firstrose.automata.configuration;

import com.firstrose.automata.annotation.EnableAutomataKafka;
import com.firstrose.automata.properties.kafka.KafkaProperties;
import com.firstrose.automata.properties.kafka.ListenerContainerFactoryProperties;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.support.serializer.JsonDeserializer;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
public class KafkaListenerContainerFactoryAutoConfiguration extends BaseKafkaAutoConfiguration {

    private Map<String, Object> createConsumerConfig(ListenerContainerFactoryProperties lcfProperties, Class keyDeserializerClass, Class valueDeserializerClass) {
        Map<String, Object> config = new HashMap<>();
        config.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, lcfProperties.getBootstrapServers());
        if(lcfProperties.getGroupId()!=null)
            config.put(ConsumerConfig.GROUP_ID_CONFIG, lcfProperties.getGroupId());
        config.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, keyDeserializerClass);
        config.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, valueDeserializerClass);
        if(lcfProperties.getAutoOffsetRest()==null || lcfProperties.getAutoOffsetRest().isEmpty())
            config.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        else
            config.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, lcfProperties.getAutoOffsetRest());
        return config;
    }

    private Deserializer prepareConsumerKeyDeserializerObject(Class keyDeserializerClass, String keyJsonType) throws IllegalAccessException, InstantiationException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException {

        Deserializer keyDeserializerObject = null;
        if(keyDeserializerClass.equals(StringDeserializer.class))
            keyDeserializerObject = StringDeserializer.class.getDeclaredConstructor().newInstance();
        else if(keyDeserializerClass.equals(JsonDeserializer.class))
            keyDeserializerObject = JsonDeserializer.class.getConstructor(Class.class).newInstance(Class.forName(keyJsonType));
        return keyDeserializerObject;
    }

    private Deserializer prepareConsumerValueDeserializerObject(Class valueDeserializerClass, String valueJsonType) throws IllegalAccessException, InstantiationException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException {
        Deserializer valueDeserializerObject = null;
        if(valueDeserializerClass.equals(StringDeserializer.class))
            valueDeserializerObject = StringDeserializer.class.getDeclaredConstructor().newInstance();
        else if(valueDeserializerClass.equals(JsonDeserializer.class))
            valueDeserializerObject = JsonDeserializer.class.getConstructor(Class.class).newInstance(Class.forName(valueJsonType));
        return valueDeserializerObject;
    }

    public void init() throws ClassNotFoundException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        // If automata kafka properties is not exists in application properties then complete init
        KafkaProperties kafkaProperties = automataProperties.getKafka();
        if(kafkaProperties == null)
            return;

        List<ListenerContainerFactoryProperties> listenerContainerFactoryProperties = kafkaProperties.getListenerContainerFactories();
        if(listenerContainerFactoryProperties == null)
            return;

        // Continue only if any automata kafka exists in application properties
        String[] beanNames = applicationContext.getBeanNamesForAnnotation(EnableAutomataKafka.class);
        List<EnableAutomataKafka> annotations = searchAnnotation(beanNames);
        for(ListenerContainerFactoryProperties lcfProperties : kafkaProperties.getListenerContainerFactories()) {
            String lcfBeanName = lcfProperties.getName();
            Boolean filterIsOn = filterIsOn(annotations);
            Boolean isExist = beanIsExist(lcfBeanName, annotations);

            if(beanNames.length>0 && (!filterIsOn || isExist)) {
                log.info("Registering kafka listener container '{}'", lcfBeanName);
                String keyDeserializer = lcfProperties.getKeyDeserializer();
                String valueDeserializer = lcfProperties.getValueDeserializer();
                String keyJsonType = lcfProperties.getKeyJsonType();
                String valueJsonType = lcfProperties.getValueJsonType();
                Class keyDeserializerClass = Class.forName(keyDeserializer);
                Class valueDeserializerClass = Class.forName(valueDeserializer);

                // Create consumer config
                Map<String, Object> config = createConsumerConfig(lcfProperties, keyDeserializerClass, valueDeserializerClass);

                // Prepare key and value deserializer
                Deserializer keyDeserializerObject = prepareConsumerKeyDeserializerObject(keyDeserializerClass, keyJsonType);
                Deserializer valueDeserializerObject = prepareConsumerValueDeserializerObject(valueDeserializerClass, valueJsonType);

                // Create listener container factory
                ConcurrentKafkaListenerContainerFactory lcfObject = new ConcurrentKafkaListenerContainerFactory();
                lcfObject.setConsumerFactory(new DefaultKafkaConsumerFactory(config, keyDeserializerObject, valueDeserializerObject));
                populateObject(lcfBeanName, lcfObject);
            }
        }
    }
}
