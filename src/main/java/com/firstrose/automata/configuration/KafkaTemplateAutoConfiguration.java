package com.firstrose.automata.configuration;

import com.firstrose.automata.annotation.EnableAutomataKafka;
import com.firstrose.automata.properties.kafka.KafkaProperties;
import com.firstrose.automata.properties.kafka.TemplateProperties;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
public class KafkaTemplateAutoConfiguration extends BaseKafkaAutoConfiguration {

    public void init() throws ClassNotFoundException {
        String[] beanNames = applicationContext.getBeanNamesForAnnotation(EnableAutomataKafka.class);
        List<EnableAutomataKafka> annotations = searchAnnotation(beanNames);

        KafkaProperties kafkaProperties = automataProperties.getKafka();
        if(kafkaProperties==null)
            return;
        for(TemplateProperties tProperties : kafkaProperties.getTemplates()) {
            String tBeanName = tProperties.getName();
            Boolean filterIsOn = filterIsOn(annotations);
            Boolean isExist = beanIsExist(tBeanName, annotations);

            if(beanNames.length>0 && (!filterIsOn || isExist)) {
                log.info("Registering kafka template '{}'", tBeanName);

                // Create producer config
                String keySerializer = tProperties.getKeySerializer();
                String valueSerializer = tProperties.getValueSerializer();
                Class keySerializerClass = Class.forName(keySerializer);
                Class valueSerializerClass = Class.forName(valueSerializer);
                Map<String, Object> config = new HashMap<>();
                config.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, tProperties.getBootstrapServers());
                if(tProperties.getClientId()!=null)
                    config.put(ProducerConfig.CLIENT_ID_CONFIG, tProperties.getClientId());
                else
                    config.put(ProducerConfig.CLIENT_ID_CONFIG, tProperties.getName());
                config.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, keySerializerClass);
                config.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, valueSerializerClass);

                // Create kafka template
                KafkaTemplate tObject = new KafkaTemplate(new DefaultKafkaProducerFactory(config));
                populateObject(tBeanName, tObject);
            }
        }
    }
}
